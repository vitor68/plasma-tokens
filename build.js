const StyleDictionary = require('style-dictionary')
const baseConfig = require('./config.json')
// const Color = require('tinycolor2')
const _ = require('lodash')

StyleDictionary.registerTransform({
    name: 'name/cti/bem',
    type: 'name',
    transformer: function(prop, options) {
        let result = '';
        if (prop.path.length == 2) {
            result = prop.path.join('--');
            console.log("result: ", result);
        } else if (prop.path.length == 3) {
            result = `${prop.path[0]}__${prop.path[1]}--${prop.path[2]}`;
            console.log("result: ", result);
        } else if (prop.path.length == 4) {
            result = `${prop.path[0]}__${prop.path[1]}-${prop.path[2]}--${prop.path[3]}`;
            console.log("result: ", result);
        } 
        return result;
    },
})

StyleDictionary.registerFormat({
    name: `outputReferencesJS`,
    formatter: function({ dictionary }) {
      return dictionary.allTokens.map(token => {
        let value = JSON.stringify(token.value);
        // the `dictionary` object now has `usesReference()` and
        // `getReferences()` methods. `usesReference()` will return true if
        // the value has a reference in it. `getReferences()` will return
        // an array of references to the whole tokens so that you can access
        // their names or any other attributes.
        if (dictionary.usesReference(token.original.value)) {
          const refs = dictionary.getReferences(token.original.value);
          refs.forEach(ref => {
            value = value.replace(ref.value, function() {
              return `${ref.name}`;
            });
          });
        }
        return `export const ${token.name} = ${value};`
      }).join(`\n`)
    }
  })

const StyleDictionaryExtended = StyleDictionary.extend(baseConfig)

StyleDictionaryExtended.buildAllPlatforms()